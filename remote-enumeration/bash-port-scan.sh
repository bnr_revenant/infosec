
#!/bin/bash
# may require a tty shell use:  python(2|3) -c 'import pty;pty.spawn("/bin/bash")';

host=$1
spt=`basename "$0"`

# POSIX Shell
# if expr "$host    " : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$' >/dev/null; then
if [[ $host =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
    echo "[+] $host is a valid ip"
else
    echo "[!] usage: ./$spt <-ip-address->"
    exit 1
fi

# for port in {1..65535}; do # non posix
# POSIX loop
p=0
while [ $p -le 65535 ]; do
	timeout .1 /bin/bash -c "echo >/dev/tcp/$host/$p"
	if [ $? -eq 0 ]; then
		echo "[+] port $p is open"
	fi
    p=$((++p))
done

echo "[!] Done"
