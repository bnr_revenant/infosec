
$domainObj = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
$PDC = ($domainObj.PdcRoleOwner).Name
$SearchString = "LDAP://"
$SearchString += $PDC + "/"
$DistinguishedName = "DC=$($domainObj.Name.Replace('.', ',DC='))"
$SearchString += $DistinguishedName

$Searcher = New-Object System.DirectoryServices.DirectorySearcher([ADSI]$SearchString)
$objDomain = New-Object System.DirectoryServices.DirectoryEntry
$Searcher.SearchRoot = $objDomain

$Searcher.filter="(ObjectClass=Group)"
$Result = $Searcher.FindAll()

$names = $Result | select -expandproperty Properties | foreach { $_.name }
$object = $Result | select -expandproperty Properties | foreach { $_ } 

foreach ($n in $names)
{
    foreach ($m in $Object)
    {
        $gm = ($m.member -split ',' | ConvertFrom-StringData).CN | select -unique
        foreach ($item in $gm)
        {
            if ("$n" -eq "$item") 
            {write-output "[!] Group Name : $($m.name)  | has member : $($item)"}

        }
    }
}
