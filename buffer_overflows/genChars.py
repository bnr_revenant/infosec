#!/usr/bin/python3

import sys

# 256 in hex is FF
# this will generate all possible byte character values from 0 to 256
for x in range(0,256):
 sys.stdout.write("\\x" + '{:02x}'.format(x))

